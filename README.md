> Esse projeto ainda está em teste e a publicações ainda pode estar quebrada

# Design System do Governo Federal - Web Components - Quickstart React

## Descrição

Projeto exemplificando como fazer uso da [biblioteca de Web Components do Design System GOV.BR](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc 'Biblioteca de Web Components do Design System GOV.BR') em conjunto com o [React](https://reactjs.org/ 'React').

Para mais detalhes da biblioteca, acesse o [nosso repositório](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc 'Biblioteca de Web Components do Design System GOV.BR')

Para utilizar os web components do Design System GOV.BR dentro de um projeto do Angular basta seguir os passos abaixo:

## Tecnologias

Esse projeto é desenvolvido usando:

1. [Biblioteca de Web Components do Design System GOV.BR](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc 'Biblioteca de Web Components do Design System GOV.BR')
1. [React](https://reactjs.org/ 'React').

Para saber mais detalhes sobre Web Components sugerimos que consulte o [MDN](https://developer.mozilla.org/pt-BR/docs/Web/Web_Components 'Web Components | MDN').

## Dependências

As principais dependências do projeto são:

1. [Design System GOV.BR](https://www.gov.br/ds/ 'Design System do Governo Federal')

1. [Web Components](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/ "Design System GOV.BR Web Components")

1. [Font Awesome](https://fontawesome.com/ 'Font Awesome')

1. [Fonte Rawline](https://www.cdnfonts.com/rawline.font/ 'Fonte Rawline')

> O fontawesome e a fonte rawline podem ser importadas de um CDN. Consulte a documentação no site do Design System do Governo Federal para mais detalhes

## Como executar o projeto?

```sh
git clone git@gitlab.com:govbr-ds/govbr-ds-webcomponents-quickstart-react.git
```

```sh
npm install
```

```sh
npm start
```

Após isso o projeto vai estar disponível no endereço `http://localhost:3000/`.

OBS: Para contribuir com o projeto o clone pode não ser a maneira correta. Por favor consulte nossos guias sobre como contribuir na nossa [wiki](https://govbr-ds.gitlab.io/govbr-ds-wiki/ 'Wiki').

### Explicando

Para usar os Web Components Design System GOV.BR com o React é preciso seguir os seguintes passos:

#### declarations.d.ts

Na pasta _src_ do seu projeto adicione o arquivo `declarations.d.ts` e o código abaixo:

```typescript
declare namespace JSX {
  interface IntrinsicElements {
    "br-header": any;
    "br-footer": any;
    "br-menu": any;
    "br-input": any;
    "br-checkbox": any;
  }
}

declare module '@govbr/dsgov-webcomponents/dist/dsgov.common'
```

Esse passo registra os Web Components para o React. Registre os componentes necessários de acordo com a documentação da [biblioteca de Web Components do Design System GOV.BR](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc 'Biblioteca de Web Components do Design System GOV.BR').

Alguns componentes são divididos em subcomponentes, como por exemplo o _Header_ e o _Footer_. Nesses casos é necessário ter um arquivo de _declarations.d.ts_ definindo os elementos para cada componente. Por favor consulte nosso código de exemplo para esses componentes para entender como fazer.

#### App.js

Inclua essas duas importações no arquivo _App.js_.

```javascript
import '@govbr-ds/core/dist/core.min.css'
import '@govbr-ds/webcomponents/dist/webcomponents.umd'
```

## Precisa de ajuda?

> Por favor **não** crie issues para fazer perguntas...

Use nossos canais abaixo para obter tirar suas dúvidas:

-   Site do Design System GOV.BR <https://www.gov.br/ds/>

-   Web Components (versão estável) <https://govbr-ds.gitlab.io/govbr-ds-webcomponents/main>

-   Web Components (versão em desenvolvimento\*) <https://govbr-ds.gitlab.io/govbr-ds-webcomponents/develop>

-   Pelo nosso email <govbr-ds@serpro.gov.br>

-   Usando nosso canal no discord <https://discord.gg/U5GwPfqhUP>

## Como contribuir?

Por favor verifique nossos guias de [como contribuir](./CONTRIBUTING.md 'Como contribuir?').

### Commits

Nesse projeto usamos um padrão para branches e commits. Por favor observe a documentação na nossa [wiki](https://govbr-ds.gitlab.io/govbr-ds-wiki/ 'Wiki') para aprender sobre os nossos padrões.

## Créditos

Os Web Components do Design System do Governo Federal são criados pelo [SERPRO](https://www.serpro.gov.br/ 'SERPRO | Serviço Federal de Processamento de Dados') e [Dataprev](https://www.dataprev.gov.br/ 'Dataprev | Empresa de Tecnologia e Informações da Previdência') juntamente com a participação da comunidade.

## Licença

Nesse projeto usamos a licença MIT.
