declare namespace JSX {
  interface IntrinsicElements {
    "br-header": any;
    "br-footer": any;
    "br-menu": any;
    "br-input": any;
    "br-checkbox": any;
  }
}
