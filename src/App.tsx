import "@govbr-ds/webcomponents/dist/webcomponents.umd.min";
import "@govbr/dsgov/dist/core.css";
import React, { Fragment } from "react";
import Footer from "./components/Footer";
import Header from "./components/Header";
import Menu from "./components/Menu";
import Routes from "./routes";

const App = () => {
  return (
    <Fragment>
      <Header />
      <main id="main" className="d-flex flex-fill">
        <div className="container-lg">
          <div className="row">
            <div className="col-sm-4 col-md-8 col-lg-3 px-0">
              <div className="col-md-12 pt-1 pb-3">
                <Menu />
              </div>
            </div>
            <div className="col pt-3 pb-5">
              <div className="main-content pl-sm-3" id="main-content">
                <Routes />
              </div>
            </div>
          </div>
        </div>
      </main>
      <Footer />
    </Fragment>
  );
};

export default App;
