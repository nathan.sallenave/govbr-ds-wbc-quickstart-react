import React, { useState } from "react";
import Checkbox from "../components/Checkbox";
import Input from "../components/Input";

const Inputs = () => {
  const [inputValue, setInputValue] = useState("123");

  const [checkboxesValues, setCheckboxesValues] = useState({
    ckbUnchecked: false,
    ckbChecked: true,
    ckbValid: false,
    ckbInvalid: false,
    ckbDisabled: false,
  });

  const handleCkbChange = (event: any, value: any) => {
    const newValues = { ...checkboxesValues, [event.target.name]: value };
    setCheckboxesValues(newValues);
  };

  const { ckbUnchecked, ckbChecked, ckbInvalid, ckbValid, ckbDisabled } =
    checkboxesValues;

  return (
    <>
      <h1>Exemplos de Inputs</h1>
      <div className="row">
        <div className="col-sm-12">
          <Input value={inputValue} onChange={(v: any) => setInputValue(v)} />
          <br />
          <p>Valor digitado: {inputValue}</p>
        </div>
      </div>
      <hr />
      <div className="row">
        <div className="col-sm-12">
          <p className="label mb-0">Testando br-checkbox</p>
          <Checkbox
            name="ckbUnchecked"
            onChange={handleCkbChange}
            label="Unchecked"
            inline
            checked={ckbUnchecked && ckbUnchecked === true ? true : null}
            value={ckbUnchecked}
          />
          <Checkbox
            onChange={handleCkbChange}
            label="Checked"
            inline
            checked={ckbChecked && ckbChecked === true ? true : null}
            value={ckbChecked}
            name="ckbChecked"
          />
          <Checkbox
            onChange={handleCkbChange}
            label="Valid"
            format="valid"
            inline
            checked={ckbValid && ckbValid === true ? true : null}
            value={ckbValid}
            name="ckbValid"
          />
          <Checkbox
            onChange={handleCkbChange}
            label="Invalid"
            format="invalid"
            inline
            checked={ckbInvalid && ckbInvalid === true ? true : null}
            value={ckbInvalid}
            name="ckbInvalid"
          />
          <Checkbox
            onChange={handleCkbChange}
            label="Disabled"
            disabled
            inline
            checked={ckbDisabled && ckbDisabled === true ? true : null}
            value={ckbDisabled}
            name="ckbDisabled"
          />
          <p className="label mb-0">
            {`Valores dos checkboxes: Checked=${ckbChecked}, Unchecked=${ckbUnchecked}, Valid=${ckbValid}, Invalid=${ckbInvalid}, Disabled=${ckbDisabled}`}
          </p>
        </div>
      </div>
    </>
  );
};

export default Inputs;
