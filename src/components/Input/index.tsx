import { createRef, useEffect } from "react";

interface IBrInputProps {
  onChange: Function;
  value: any;
}

const BrInput = (props: IBrInputProps) => {
  const { onChange, value } = props;

  const inputRef = createRef<any>();

  useEffect(() => {
    inputRef.current.addEventListener("input", (e: any) => {
      if (e && e.detail) {
        if (onChange) {
          onChange(e.detail[0]);
        }
      }
    });
  }, [inputRef, onChange]);

  return <br-input value={value} ref={inputRef}></br-input>;
};

export default BrInput;
