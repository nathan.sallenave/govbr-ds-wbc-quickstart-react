const HeaderActions = () => {
  return (
    <div slot="headerAction">
      <br-header-action
        has-login
        label-login="Entrar"
        image-avatar-login="https://picsum.photos/id/823/400"
        name-avatar-login="Jhon Doe"
        title-links="Acesso Rápido"
        title-functions="Funcionalidades do Sistema"
        list-links="[
        {
          name: 'DSGOV',
          url: 'https://dsgov.estaleiro.serpro.gov.br/',
          target: '_blank'
        },
        {
          name: 'Dataprev',
          url: 'https://dataprev.gov.br/',
          target: '_blank'
        },
        {
          name: 'Serpro',
          url: 'https://www.serpro.gov.br/',
          target: '_blank'
        }
      ]"
        list-functions="[
        {
          icon: 'chart-bar',
          name: 'Funcionalidade 1',
          url: '#'
        },
        {
          icon: 'headset',
          name: 'Funcionalidade 2',
          url: '#'
        },
        {
          icon: 'comment',
          name: 'Funcionalidade 3',
          url: '#'
        }
      ]"
      ></br-header-action>
    </div>
  )
}

export default HeaderActions
