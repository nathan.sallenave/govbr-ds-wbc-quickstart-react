import HeaderActions from './HeaderActions'

const Header = () => {
  return (
    <br-header
      image="https://cdn.dsgovserprodesign.estaleiro.serpro.gov.br/design-system/images/logo-positive.png"
      signature="Dataprev | Serpro"
      title="DSGOV WebComponents"
      subtitle="DSGOV.BR - Web Components - Quickstart React"
    >
      <HeaderActions />
    </br-header>
  )
}

export default Header
