const Menu = () => {
  const menuItems = `[
    {
      'id': 1,
      'icon': 'bell',
      'name': 'Exemplos',
      'list': [
        {
          'icon': 'list',
          'name': 'Inputs',
          'url': '/inputs'
        }
      ]
    },
     {
      'id': 2,
      'icon': 'bell',
      'name': 'Nive 1',
      'list': [
        {
          'icon': 'address-book',
          'name': 'Nível 2',
          'url': 'javascript:void(0)',
          'list': [
            {
              'icon': 'tree',
              'name': 'Nível 3',
              'url': 'javascript:void(0)',
              'list': [
                {
                  'icon': 'tree',
                  'name': 'Nível 4',
                  'url': 'javascript:void(0)'
                },
                {
                  'icon': 'tree',
                  'name': 'Nível 4'
                }
              ]
            },
            {
              'icon': 'tree',
              'name': 'Nível 3',
              'url': 'javascript:void(0)'
            }
          ]
        },
        {
          'icon': 'heart',
          'name': 'Nível 2',
          'url': 'javascript:void(0)'
        }
      ]
    },
    {
      'id': 3,
      'icon': 'bell',
      'name': 'Menu Group 3'
    }
  ]`;

  return (
    <br-menu class="pt-3 pb-5" show-icon show-menu list={menuItems} is-push />
  );
};

export default Menu;
