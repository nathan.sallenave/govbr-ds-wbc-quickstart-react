import React, { createRef, useEffect } from "react";

export interface ICheckboxProps {
  label?: string;
  disabled?: Boolean;
  format?: "valid" | "invalid";
  inline?: Boolean;
  ariaLabel?: string;
  name?: string;
  checked?: Boolean | null;
  indeterminate?: Boolean;
  value?: string | Number | Boolean;
  onClick?: Function;
  onChange?: Function;
}

const Checkbox = (props: ICheckboxProps) => {
  const { onChange, value, name, ...rest } = props;
  const ckbRef = createRef<any>();

  useEffect(() => {
    const ckbCurrentRef = ckbRef.current;

    const handleChange = (e: any) => {
      onChange && onChange(e, !value);
    };

    if (ckbCurrentRef) {
      ckbCurrentRef.addEventListener("input", handleChange);
    }

    return () => {
      ckbCurrentRef.removeEventListener("input", handleChange);
    };
  }, [ckbRef, onChange, value]);

  return (
    <br-checkbox {...rest} ref={ckbRef} name={name} value={value}></br-checkbox>
  );
};

export default Checkbox;
