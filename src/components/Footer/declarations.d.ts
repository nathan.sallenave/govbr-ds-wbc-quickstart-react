declare namespace JSX {
  interface IntrinsicElements {
    'br-footer': any
    'br-footer-social-network': any
    'br-footer-social-network-item': any
    'br-footer-logo': any
    'br-footer-image': any
  }
}
