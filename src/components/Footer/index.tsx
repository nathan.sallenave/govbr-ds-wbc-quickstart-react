const Footer = () => {
  return (
    <br-footer
      text="Todo o conteúdo deste site está publicado sob a licença Creative Commons"
      categories="[
                    {
                      title: 'Categoria 1',
                      items: [
                        {title: 'item 1', href: '/item1'},
                        {title: 'item 2', href: '/item1'},
                        {title: 'item 3', href: '/item1'},
                        {title: 'item 4', href: '/item1'}
                      ]
                    },
                    {
                      title: 'Categoria 2',
                      items: [
                        {title: 'item 5', href: '#'},
                        {title: 'item 6', href: '/item1'},
                        {title: 'item 7', href: '/item1'},
                        {title: 'item 8', href: '/item1'}
                      ]
                    }
                  ]"
    >
      <br-footer-social-network label="Redes Sociais" slot="redesSociais">
        <br-footer-social-network-item url="#">
          <span className="fab fa-facebook" />
        </br-footer-social-network-item>
        <br-footer-social-network-item url="#">
          <span className="fab fa-twitter" />
        </br-footer-social-network-item>
        <br-footer-social-network-item url="#">
          <span className="fab fa-instagram" />
        </br-footer-social-network-item>
        <br-footer-social-network-item url="#">
          <span className="fab fa-linkedin" />
        </br-footer-social-network-item>
      </br-footer-social-network>

      <br-footer-logo
        src="https://cdn.dsgovserprodesign.estaleiro.serpro.gov.br/design-system/images/logo-negative.png"
        alt="Acesso à informação"
        slot="logo"
        centered="false"
      />
      <br-footer-image
        src="https://cdn.dsgovserprodesign.estaleiro.serpro.gov.br/design-system/images/logo-assign-negative.png"
        alt="Acesso à informação"
        href="#acesso-a-informacao1"
        slot="footerImagens"
      />
      <br-footer-image
        src="https://cdn.dsgovserprodesign.estaleiro.serpro.gov.br/design-system/images/logo-assign-negative.png"
        alt="Acesso à informação"
        href="#acesso-a-informacao2"
        slot="footerImagens"
      />
    </br-footer>
  )
}

export default Footer
