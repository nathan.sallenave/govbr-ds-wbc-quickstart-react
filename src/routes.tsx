import React from "react";
import { Route, Routes } from "react-router-dom";
import Dashboard from "./pages/dashboard";
import Inputs from "./pages/inputs";

const AppRoutes = () => {
  return (
    <Routes>
      <Route path="/" element={<Dashboard />} />
      <Route path="inputs" element={<Inputs />} />
    </Routes>
  );
};

export default AppRoutes;
